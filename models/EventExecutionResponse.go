package models

import "time"

type EventExecutionResponse struct {
	ResultCode EventExecutionResultCode  `json:"ResultCode"`
	ExecutionTime time.Time `json:"ExecutionTime"`
	Message string `json:"Message"`
}
