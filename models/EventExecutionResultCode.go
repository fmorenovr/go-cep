package models

type EventExecutionResultCode struct {
	IsSuccess bool `json:"IsSuccess"`
	Code string `json:"Code"`
}
