package common

import (
	"encoding/json"
	"os"
	"log"
)

type configuration struct {
	Port string
}

var AppConfig configuration

func Init(){
	loadConfiguration()
}

func loadConfiguration(){
	file, err := os.Open("config.json")
	defer file.Close()
	if err != nil {
		log.Fatalf("[loadConfig]: %s\n", err)
	}
	decoder := json.NewDecoder(file)
	AppConfig = configuration{}
	err = decoder.Decode(&AppConfig)
	if err != nil {
		log.Fatalf("[loadAppConfig]: %s\n", err)
	}
}