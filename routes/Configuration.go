package routes

import (
	"github.com/kataras/iris"
	"../controllers"
)

func RegisterRoutes(api *iris.Framework) *iris.Framework{

	api.Post("/event/fire", controllers.FireEvent)
	return api
}

